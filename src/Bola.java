/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author ASUS
 */
public class Bola {
    private double jariJari;
    final float pi = 3.14f;
    
    //set jari jari
    public void setJariJari(float a){
        jariJari = a;
    }
    
    //show diameter
    public void showDiameter(){
        System.out.println("Diameter : " +(2*jariJari)+" cm");
        
    }
    //show luas permukaan
    public void showLuasPermukaan(){
        System.out.println("Luas Permukaan : " +(4*pi*jariJari*jariJari)+" cm^2");
        
    }
    
    //showVolume
    public void showVolume(){
        System.out.println("Volume : " +(4/3*pi*jariJari*jariJari*jariJari)+" cm^3");
        
    }
}
